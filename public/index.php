<?php
ob_start();
session_start();

if(empty($_SESSION['CSRF_Token']))

    {
        $_SESSION['CSRF_Token'] = md5(uniqid(mt_rand(),true));
    }


define('ENV', 'development'); 


require __DIR__ . '/../config/functions.php';
require __DIR__ . '/../config/connect.php';
require __DIR__ . '/../config/escape.php';
require __DIR__ . '/../Classes/Interfaces/ILogger.php';
require __DIR__ . '/../Classes/DatabaseLogger.php';
require __DIR__ . '/../Classes/FileLogger.php'; 


$allowed = ['about', 'contact', 'register', 'index', 
            'featured', 'submitted', 'login', 'logout',
            'profile',
            'processLogin','processLogout','processRegister','detail',
             'viewCart', 'handleCart', 'removeItems', 'checkout', 
             'handleCheckout','ordersuccess'];



if(empty($_GET['p'])) {

$page = 'home';

} elseif(in_array($_GET['p'], $allowed)) {

$page = $_GET['p'];

} else {

$page = 'not_found';

}

if($page === 'index'){

$page= 'home';

}

$DatabaseLogger = new DatabaseLogger($dbh);
logEvent($DatabaseLogger);
$file = __DIR__ . '/../logs/event.log';
$fh = fopen($file, 'a');
$FileLogger = new FileLogger($fh);
logEvent($FileLogger);
$path = __DIR__ . '/../controller/' . $page . '.php';
require($path);