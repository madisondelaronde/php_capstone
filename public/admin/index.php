<?php

ob_start();
session_start();

require __DIR__ . '/../../config/functions.php';
require __DIR__ . '/../../config/connect.php';
require __DIR__ . '/../../config/escape.php';
require __DIR__ . '/../../Classes/Interfaces/ILogger.php';
require __DIR__ . '/../../Classes/DatabaseLogger.php';
require __DIR__ . '/../../Classes/FileLogger.php'; 


if(empty($_SESSION['admin'])) {
    $_SESSION['flash']['error'] = 'please sign in to view admin page';
    header('Location: /?p=login');
    die;
}

$DatabaseLogger = new DatabaseLogger($dbh);
logEvent($DatabaseLogger);
$file = __DIR__ . '/../../logs/event.log';
$fh = fopen($file, 'a');
$FileLogger = new FileLogger($fh);
logEvent($FileLogger);

// get data from db
global $dbh;
$query = "SELECT event FROM log
ORDER by created_at DESC limit 10";
    $stmt = $dbh->prepare($query);
    $stmt->execute();
    $DBlog = $stmt->fetchAll();

$allProducts =getAllProducts($dbh);
if(!empty($_GET['search'])){
    $allProducts = getProductbyTitle($dbh,$_GET['search']);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Page</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <style>
        .row div {
            border: 1px solid #cfcfcf;
        }
        h1, .search{
            font-weight: bold;
            font-family: "Kanit", sans-serif;
            margin-bottom: 10px;
            text-align: center;
        }

        nav li, a {
            display: inline-block;
            padding: 2px;
            text-decoration: none;
            list-style-type: none;
            font-size: 1.1em;
            margin: 15px;
            margin-top: 0;
            font-weight: bold;
            margin-bottom: 0;

        }
        nav {
            text-align: center;
        }
        .button {
            margin: 10px;
            padding: 5px;
            border: none;
            border-radius: 20px;
            background-color: lightgrey;
            font-weight: bold;
            cursor: pointer;
            width: 150px;
            display: inline-block;
            }
            .container {
                margin: 0 auto;
            }

    </style>
</head>
<body>

<h1>Admin Dashboard</h1>

<nav>
<ul>

<li><a href="/admin">Home</a></li>
<li><a href="/admin/orders.php">Orders</a></li>
<li><a href="?p=redirect">Log</a></li>
<li><a href="/admin/users.php">Users</a></li>

</ul>
</nav>

<div class="search">
    <form action="/admin">
        <!-- <input type="hidden" name="p" value="featured"> -->
      <input id="search" name="search" type="text" placeholder="Search Products">
      <input class="submit" id="submit" type="submit" value="Search">
    </form>
    </div>

<div class="container">

<div class="row">
        <div class="col-12">
        <table class="table">
        <thead>
            <tr>
            <th scope="col">Products</th>
            </tr>
            <input type="submit" class="button" value="Add Product">
            <input type="submit" class="button" value="Edit Product">
            <input type="submit" class="button" value="Delete Product">

            <tr>
                <th>ID</th>
                <th>title</th>
                <th>category</th>
                <th>price</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($allProducts as $row) :?>
            <tr>
                <td><?=e($row['id'])?></td>
                <td><?=e($row['title'])?></td>
                <td><?=e($row['category'])?></td>
                <td>$<?=e($row['price'])?></td>
            </tr>
       <?php endforeach; ?>
        </tbody>
        </table>
    </div>

</div>

<br/>
<br/>


    <div class="row">
        <div class="col-12">
        <table class="table">
        <thead>
            <tr>
            <th scope="col">Recent User Log Entries</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($DBlog as $DBitem) : ?>
            <?php foreach($DBitem as $key => $value) : ?>
            <tr><td><?=esc(ucwords($value))?></td></tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
        </tbody>
        </table>
    </div>

</div>
    
</body>
</html>