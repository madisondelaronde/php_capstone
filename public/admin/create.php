<?php

ob_start();
session_start();

require __DIR__ . '/../../config/functions.php';
require __DIR__ . '/../../config/connect.php';
require __DIR__ . '/../../config/escape.php';
require __DIR__ . '/../../Classes/Interfaces/ILogger.php';
require __DIR__ . '/../../Classes/DatabaseLogger.php';
require __DIR__ . '/../../Classes/FileLogger.php'; 


if(empty($_SESSION['admin'])) {
    $_SESSION['flash']['error'] = 'please sign in to view admin page';
    header('Location: /?p=login');
    die;
}

$DatabaseLogger = new DatabaseLogger($dbh);
logEvent($DatabaseLogger);
$file = __DIR__ . '/../../logs/event.log';
$fh = fopen($file, 'a');
$FileLogger = new FileLogger($fh);
logEvent($FileLogger);



$query = "SELECT 
users.first_name,
users.last_name,
users.email,
invoice.created_at,
invoice.price,
invoice.pst + invoice.gst as tax,
line_items.quantity as items
from 
invoice
join users on invoice.user_id = users.id
join line_items on invoice.id = line_items.invoice_id
order by created_at desc";

$stmt = $dbh->prepare($query);
$stmt->execute();
$allOrder = $stmt->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Page</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <style>
        .row div {
            border: 1px solid #cfcfcf;
        }
        h1, .search{
            font-weight: bold;
            font-family: "Kanit", sans-serif;
            margin-bottom: 10px;
            text-align: center;
        }

        nav li, a {
            display: inline-block;
            padding: 2px;
            text-decoration: none;
            list-style-type: none;
            font-size: 1.1em;
            margin: 15px;
            margin-top: 0;
            font-weight: bold;
            margin-bottom: 0;

        }
        nav {
            text-align: center;
        }
        .button {
            margin: 10px;
            padding: 5px;
            border: none;
            border-radius: 20px;
            background-color: lightgrey;
            font-weight: bold;
            cursor: pointer;
            width: 150px;
            display: inline-block;
            }
            .container {
                margin: 0 auto;
            }

    </style>
</head>
<body>

<h1>Add new Product</h1>

<nav>
<ul>

<li><a href="/admin">Home</a></li>
<li><a href="/admin/orders.php">Orders</a></li>
<li><a href="?p=redirect">Log</a></li>
<li><a href="/admin/users.php">Users</a></li>


</ul>
</nav>


<div class="container">

    <div class="row">
            <div class="col-12">
            <form 
                name="productAdd"
                id="productAdd"
                action="/admin/create"
                method="post"
                autocomplete = "on"
                enctype="multipart/form-data"
                novalidate>

                <p><label for="first_name">First Name:</label>
                <input type="text" name="first_name" value="<?=e($post['first_name'] ?? '')?>">
                <span><?=e($errors['first_name'][0] ?? '')?></span></p> 

                <p><label for="last_name">Last Name:</label>
                <input type="text" name="last_name" value="<?=e($post['last_name'] ?? '')?>">
                <span><?=e($errors['last_name'][0] ?? '')?></span></p> 

                <p><label for="street">Street:</label>
                <input type="text" name="street" value="<?=e($post['street'] ?? '')?>">
                <span><?=e($errors['street'][0] ?? '')?></span></p> 

                <p><label for="city">City:</label>
                <input type="text" name="city" value="<?=e($post['city'] ?? '')?>">
                <span><?=e($errors['city'][0] ?? '')?></span></p> 
                <p><label for="province">Province:</label>
                
                <input type="text" name="province" value="<?=e($post['province'] ?? '')?>">
                <span><?=e($errors['province'][0] ?? '')?></span></p> 

                <p><label for="country">Country:</label>
                <input type="text" name="country" value="<?=e($post['country'] ?? '')?>">
                <span><?=e($errors['country'][0] ?? '')?></span></p> 

                <p><label for="phone">Phone:</label>
                <input type="text" name="phone" value="<?=e($post['phone'] ?? '')?>">
                <span><?=e($errors['phone'][0] ?? '')?></span></p> 

                <p><label for="email">Email:</label>
                <input type="text" name="email" value="<?=e($post['email'] ?? '')?>">
                <span><?=e($errors['email'][0] ?? '')?></span></p>

                <p><label for="password">Password:</label>
                <input type="password" name="password" value="<?=e($post['password'] ?? '')?>">
                <span><?=e($errors['password'][0] ?? '')?></span></p>

                <p><label for="password_confirm">Password Confirm:</label>
                <input type="password" name="password_confirm" value="<?=e($post['password_confirm'] ?? '')?>">
                <span><?=e($errors['password_confirm'][0] ?? '')?></span></p>

                <p><label for="subscribe">Subscribe:</label>
                <input type="checkbox" name="subscribe" value="1">
                <span><?=e($errors['subscribe'][0] ?? '')?></span></p> 

            </form>


        </div>

    </div>

    <br/>

</div>
    
</body>
</html>