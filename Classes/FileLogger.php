<?php

class FileLogger implements ILogger
{
    // constructor
    private $fh;
    public function __construct($fh){
        $this->fh=$fh;
    }
    // write method that accepts event and writes to db
    public function write($event){
        fputs($this->fh, $event . "\n");
        fclose($this->fh);
    }
}

