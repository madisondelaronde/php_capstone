<?php

class DatabaseLogger implements ILogger
{
    private $dbh;
    public function __construct($dbh){
        $this->dbh=$dbh;
    }
    public function write($event){

      // connect to db
        // insert event to log table
      $this->dbh;
        $query = "INSERT INTO log
           (
               event
           )
           VALUES
           (
               :event
           )";

        $stmt = $this->dbh->prepare($query);

        $stmt->bindValue(':event', $event);

        // execute the query
        $stmt->execute();

        return $this->dbh->lastInsertId();
      


    }
} 