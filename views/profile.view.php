<?php

include __DIR__ . '/includes/header.php';

$flash = $_SESSION['flash']??[];
unset($_SESSION['flash']);

if(isset($_SESSION['user_id'])) {
global $dbh;
$query = "SELECT
            *
            FROM
            users
            WHERE
            id=?";

$stmt = $dbh->prepare($query);

$stmt->bindValue(1, $_SESSION['user_id'], PDO::PARAM_INT);

// execute the query
$stmt->execute();

$result = $stmt->fetch();
} else {
    $result = [];
}

?>

    <style>
        body {
            text-align: center;
            color: white;
            font-family: 'Bebas Neue', cursive;
            background-color: rgb(179, 181, 209);
        }
        p {
            list-style: none;
        }
        p {
            font-size: 20px;
        }
    </style>


    
    <h1><?=e($title)?></h1>
    <!-- <h2><?php //e($heading)?></h2> -->
    <?php require __DIR__ . '/includes/flash.inc.php'; ?>
    <ul>
        
        <p><strong>First Name</strong>: <?=e($result['first_name'])?></p>
        <p><strong>Last Name</strong>: <?=e($result['last_name'])?></p>
        <p><strong>Street</strong>: <?=e($result['street'])?></p>
        <p><strong>City</strong>: <?=e($result['city'])?></p>
        <p><strong>Postal Code</strong>: <?=e($result['postal'])?></p>
        <p><strong>Province</strong>: <?=e($result['province'])?></p>
        <p><strong>Country</strong>: <?=e($result['country'])?></p>
        <p><strong>Phone</strong>: <?=e($result['phone'])?></p>
        <p><strong>Email</strong>: <?=e($result['email'])?></p>
    
    </ul>
    
    <?php 
    include __DIR__ . ('/includes/footer.php')
    ?>
    