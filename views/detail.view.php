<?php 
    include __DIR__ . '/includes/header.php';

?>
<?php require __DIR__ . '/includes/flash.inc.php'; ?>

    <h1>Product Details</h1>

    <?php 
    
      foreach($detailData as $row):?>

      <div class="gallery">
        <div class="card">
          <img src="images/<?=esc($row['image'])?>" alt="<?=esc($row['title'])?>" width="250" height="250">          <h1><?=e($row['title'])?></h1>
            <p class="price"><?=e($row['price'])?></p>
            <p><?=e($row['excerpt'])?></p>
            <p>Category: <?=e($row['category'])?></p>
            <p>Dimensions: <?=e($row['dimensions'])?></p>
            <p><?=e($row['description'])?></p>
            <p>Quantity:<?=e($row['quantity'])?></p>
            <form method="POST" action="?p=handleCart">
            <input type="hidden" name="CSRF_Token" value="<?=getCSRFToken()?>" />

              <input type="hidden" name="id" value="<?=e($row['id'])?>">
              <input type="submit" class="add" value="Add to Cart">
          </form>        
        </div>
      </div>
      <?php endforeach; ?>

      <?php 
    include __DIR__ . ('/includes/footer.php')

      ?>