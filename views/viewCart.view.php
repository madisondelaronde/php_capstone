<?php

include __DIR__ . ('/includes/header.php')


?>

<?php

include __DIR__ . ('/includes/flash.inc.php')


?>
    <h1>Your Cart</h1>


    <?php if(!empty($_SESSION['Cart'])) : ?>

   

            <table>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th></th>
                </tr>


                <?php foreach($_SESSION['Cart'] as $row) :?>
                <tr>
                    <td><?=esc($row['id'])?></td>
                    <td><?=esc($row['title'])?></td>  
                    <td>$<?=esc($row['price'])?></td>
                    <td><?=esc($row['quantity'])?></td>
                    <td> 
                        <form method="POST" action="?p=removeItems">
                            <input type="hidden" name="id" value="<?=e($row['id'])?>">
                            <input type="submit" class="add" value="Remove">
                        </form></td>
                </tr>
                <?php endforeach; ?>
                    
                    <tr><td>Subtotal:</td><td>$<?=esc(number_format($subtotal, 2))?></td></tr>
                    <tr><td>PST:</td><td>$<?=esc(number_format($pst, 2))?></td></tr>
                    <tr><td>GST:</td><td>$<?=esc(number_format($gst, 2))?></td></tr>
                    <tr><td>Total:</td><td>$<?=esc(number_format($total, 2))?></td></tr>

            </table>

        

   <div class="links">
        <p><a href="?p=featured">Continue Shopping</a></p>
        <p><a href="?p=removeItems&clear=1">Clear Cart</a></p>
        <p><a href="?p=checkout">Checkout Now</a></p>
   </div>

        <?php else : ?>
        <h2>your cart is empty :(</h2>
        <p class="links"><a href="?p=featured">View Collection</a></p>
    <?php endif; ?>


    <?php 
    include __DIR__ . ('/includes/footer.php')
    ?>