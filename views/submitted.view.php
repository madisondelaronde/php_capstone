<?php

// include __DIR__ . '/../config/connect.php';
// include __DIR__ . '/../config/functions.php';
include __DIR__ . '/includes/header.php';


// user id vapdation
// if(empty($_GET['id'])) { 
//     die('please provide a user id');
// }

// vapdate that id is an integer
// if(intval($_GET['id']) != $_GET['id']) {
//     die('user ID must be an integer');
// }

global $dbh;
$query = "SELECT
            *
            FROM
            users
            WHERE
            id=?";

$stmt = $dbh->prepare($query);

$stmt->bindValue(1, $_GET['id'], PDO::PARAM_INT);

// execute the query
$stmt->execute();

$result = $stmt->fetch();


$title = 'Thank you for registering!';

$heading = 'Here are your profile details:';
?>

    <style>
        body {
            text-align: center;
            color: white;
            font-family: 'Bebas Neue', cursive;
            background-color: rgb(179, 181, 209);
        }
        p {
            list-style: none;
        }
        p {
            font-size: 20px;
        }
    </style>


    
    <h1><?=e($title)?></h1>
    <h2><?=e($heading)?></h2>

    <ul>
        
        <p><strong>First Name</strong>: <?=e($result['first_name'])?></p>
        <p><strong>Last Name</strong>: <?=e($result['last_name'])?></p>
        <p><strong>Street</strong>: <?=e($result['street'])?></p>
        <p><strong>City</strong>: <?=e($result['city'])?></p>
        <p><strong>Postal Code</strong>: <?=e($result['postal'])?></p>
        <p><strong>Province</strong>: <?=e($result['province'])?></p>
        <p><strong>Country</strong>: <?=e($result['country'])?></p>
        <p><strong>Phone</strong>: <?=e($result['phone'])?></p>
        <p><strong>Email</strong>: <?=e($result['email'])?></p>
    
    </ul>
    
    <?php 
    include __DIR__ . ('/includes/footer.php')
    ?>
    