<?php 
    include __DIR__ . '/includes/header.php';

?>
<?php require __DIR__ . '/includes/flash.inc.php'; ?>

    <h1><?=$finalMsg?></h1>


    <table>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th></th>
                </tr>


                <?php foreach($invoice_items as $row) :?>
                <tr>
                    <td><?=e($row['id'])?></td>
                    <td><?=e($row['line_title'])?></td>  
                    <td><?=e($row['line_price'])?></td>
                    <td><?=e($row['quantity'])?></td>
                </tr>
                <?php endforeach; ?>
                    
                    <tr><td>Subtotal:</td><td>$<?=e(number_format($subtotal, 2))?></td></tr>
                    <tr><td>PST:</td><td>$<?=e(number_format($pst, 2))?></td></tr>
                    <tr><td>GST:</td><td>$<?=e(number_format($gst, 2))?></td></tr>
                    <tr><td>Total:</td><td>$<?=e(number_format($total, 2))?></td></tr>
            </table>


      <?php 
    include __DIR__ . ('/includes/footer.php')

        ?>

        