<?php
    include __DIR__ . '/includes/header.php';
?>
<?php require __DIR__ . '/includes/flash.inc.php'; ?>

<table>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th></th>
                </tr>


                <?php foreach($_SESSION['Cart'] as $row) :?>
                <tr>
                    <td><?=e($row['id'])?></td>
                    <td><?=e($row['title'])?></td>  
                    <td><?=e($row['price'])?></td>
                    <td><?=e($row['quantity'])?></td>
                </tr>
                <?php endforeach; ?>
                    
                    <tr><td>Subtotal:</td><td>$<?=e(number_format($subtotal, 2))?></td></tr>
                    <tr><td>PST:</td><td>$<?=e(number_format($pst, 2))?></td></tr>
                    <tr><td>GST:</td><td>$<?=e(number_format($gst, 2))?></td></tr>
                    <tr><td>Total:</td><td>$<?=e(number_format($total, 2))?></td></tr>
            </table>



<form action="?p=handleCheckout" method="POST">
<input type="hidden" name="CSRF_Token" value="<?=getCSRFToken()?>" />

    <p><label for="cardName">Name on Card:</label>
    <input type="text" name="cardName" value="<?=e($post['cardName'] ?? '')?>">
    <span><?=e($errors['cardName'][0] ?? '')?></span></p> 

    <p><label for="cardNumber">Credit Card Number:</label>
    <input type="text" name="cardNumber" value="<?=e($post['cardNumber'] ?? '')?>">
    <span><?=e($errors['cardNumber'][0] ?? '')?></span></p> 

    <p><label for="expiry">Expiry Date</label>
    <input type="text" name="expiry" value="<?=e($post['expiry'] ?? '')?>">
    <span><?=e($errors['expiry'][0] ?? '')?></span></p> 

    <p><label for="cvv">CVV:</label>
    <input type="text" name="cvv" value="<?=e($post['cvv'] ?? '')?>">
    <span><?=e($errors['cvv'][0] ?? '')?></span></p> 

    <div class="links">
        <p><a href="?p=featured">Continue Shopping</a></p>
        <input class="submit" type="submit" value="Submit" />
   </div>

</form>


<?php
include __DIR__ . '/includes/footer.php';
?>