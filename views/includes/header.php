<?php



// $flash = !empty($_SESSION['flash']) ? $_SESSION['flash'] : [];

// unset($_SESSION['flash']);

//  $flash = $_SESSION['flash'] ?? [];
//  unset($_SESSION['flash']); ?>

<!DOCTYPE html>

<html lang="en">
  <head>
  <title><?=e($title)?></title>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link rel="stylesheet" href="styles/style.css" />
<link
href="styles/print.css"
rel="stylesheet"
media="print"
type="text/css"
/>
<link rel="preconnect" href="https://fonts.googleapis.com" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
<link
href="https://fonts.googleapis.com/css2?family=Poppins&display=swap"
rel="stylesheet"
/>
</head>

  <body>
    <header>
    <div class="navbar">
<a href="index.php" class="logo">mads glass</a>
<input type="checkbox" class="nav-toggle" id="nav-toggle" />
<nav>
<ul>
<li><a href="?p=index">home</a></li>
<li><a href="?p=about">about</a></li>
<li><a href="?p=featured">featured</a></li>
<li><a href="?p=viewCart">cart 
<?php if(!empty($_SESSION['Cart'])) : ?>
  (<?=esc(count($_SESSION['Cart']))?>)
  <?php else : ?>
  (0)
  <?php endif; ?>
</a></li> 
<?php if (empty($_SESSION['user_id'])) : ?>
  <li><a href="?p=register">register</a></li>
  <li><a href="?p=login">login</a></li>
<?php else : ?>
  <li><a href="?p=profile">profile</a></li>
  <li><a href="?p=logout">logout</a></li>
<?php endif; ?>
</ul>
</nav>



<label for="nav-toggle" class="nav-toggle-label">
<span
><img class="mobileMenu" src="images/menu.png" alt="menu"/>
</span>
</label>

</div>

    </header>

