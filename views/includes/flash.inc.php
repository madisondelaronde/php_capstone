<?php if(!empty($flash['success'])) : ?>
    <div class="flash success">
        <?=e($flash['success'])?>
    </div>
<?php endif; ?>

<?php if(!empty($flash['error'])) : ?>
    <div class="flash error">
        <?=e($flash['error'])?>
    </div>
<?php endif; ?>
