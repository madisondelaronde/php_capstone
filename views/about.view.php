<?php 
    include __DIR__ . ('/includes/header.php')
    ?>
<div class="content">
      <div class="title">
        <h1>About Us</h1>
        <h2>Indigenous + Women-Owned</h2>
        <h2>
          Handcrafted stained glass art curated locally in Winnipeg, Manitoba.
        </h2>
        <img id="insta" src="./images/instalogo.png" />
      </div>

      <div class="item">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d164633.287843756!2d-97.15222505000003!3d49.85382205!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x52ea73fbf91a2b11%3A0x2b2a1afac6b9ca64!2sWinnipeg%2C%20MB!5e0!3m2!1sen!2sca!4v1638138036044!5m2!1sen!2sca"
          width="600"
          height="450"
          style="border: 0"
          allowfullscreen=""
          loading="lazy"
        ></iframe>
      </div>
    </div>
  
    <?php 
    include __DIR__ . ('/includes/footer.php')
    ?>
   