<?php

if(!empty($_SESSION['user_id'])){
  header('Location: /?p=processLogout');
  die;
}

$flash = $_SESSION['flash']??[];
unset($_SESSION['flash']);

?>
<?php require __DIR__ . ('/includes/header.php');?>
    <section>
      <h1>logout page</h1>
      
      <?php require __DIR__ . '/includes/flash.inc.php'; ?>
    </section>

    <?php
    require __DIR__ . ('/includes/footer.php')
    ?>
  