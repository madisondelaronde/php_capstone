
<?php
  
    include __DIR__ . '/includes/header.php';

?>

    <div id="contact">
      <h2>Sign Up Form</h2>
      <form  method="post" action="/?p=processRegister" novalidate>

<input type="hidden" name="CSRF_Token" value="<?=getCSRFToken()?>" />



<p><label for="first_name">First Name:</label>
<input type="text" name="first_name" value="<?=e($post['first_name'] ?? '')?>">
<span><?=e($errors['first_name'][0] ?? '')?></span></p> 

<p><label for="last_name">Last Name:</label>
<input type="text" name="last_name" value="<?=e($post['last_name'] ?? '')?>">
<span><?=e($errors['last_name'][0] ?? '')?></span></p> 

<p><label for="street">Street:</label>
<input type="text" name="street" value="<?=e($post['street'] ?? '')?>">
<span><?=e($errors['street'][0] ?? '')?></span></p> 

<p><label for="city">City:</label>
<input type="text" name="city" value="<?=e($post['city'] ?? '')?>">
<span><?=e($errors['city'][0] ?? '')?></span></p> 

<p><label for="postal">Postal Code:</label>
<input type="text" name="postal" value="<?=e($post['postal'] ?? '')?>">
<span><?=e($errors['postal'][0] ?? '')?></span></p> 

<p><label for="province">Province:</label>
<input type="text" name="province" value="<?=e($post['province'] ?? '')?>">
<span><?=e($errors['province'][0] ?? '')?></span></p> 

<p><label for="country">Country:</label>
<input type="text" name="country" value="<?=e($post['country'] ?? '')?>">
<span><?=e($errors['country'][0] ?? '')?></span></p> 

<p><label for="phone">Phone:</label>
<input type="text" name="phone" value="<?=e($post['phone'] ?? '')?>">
<span><?=e($errors['phone'][0] ?? '')?></span></p> 

<p><label for="email">Email:</label>
<input type="text" name="email" value="<?=e($post['email'] ?? '')?>">
<span><?=e($errors['email'][0] ?? '')?></span></p>

<p><label for="password">Password:</label>
<input type="password" name="password" value="<?=e($post['password'] ?? '')?>">
<span><?=e($errors['password'][0] ?? '')?></span></p>

<p><label for="password_confirm">Password Confirm:</label>
<input type="password" name="password_confirm" value="<?=e($post['password_confirm'] ?? '')?>">
<span><?=e($errors['password_confirm'][0] ?? '')?></span></p>

<p><label for="subscribe">Subscribe:</label>
<input type="checkbox" name="subscribe" value="1">
<span><?=e($errors['subscribe'][0] ?? '')?></span></p> 

<p>
    <input class="submit" type="submit" value="Submit" />
</p>


<!-- <p><button class="submit" type="submit">Submit</button></p> -->
    </form>
    </div>
  
    <?php 
    include __DIR__ . ('/includes/footer.php')
    ?>
