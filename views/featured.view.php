<?php 
    include __DIR__ . '/includes/header.php';
?>

<?php require __DIR__ . '/includes/flash.inc.php'; ?>


    <div class="category">
      <ul>
      <?php foreach($allcategories as $data) : ?>
        <?php foreach($data as $key => $value) : ?>
          <li><a href="/index.php?p=featured&category=<?=e($value)?>"><?=e($value)?></a></li>
          <?php endforeach; ?>
      <?php endforeach; ?>
      </ul>
    </div>

    <div class="search">
    <form action="/">
        <input type="hidden" name="p" value="featured">
      <input id="search" name="search" type="text" placeholder="What are you looking for?">
      <input class="submit" id="submit" type="submit" value="Search">
    </form>
    </div>


    <div class="gallery">

    <?php 
    
      foreach($allProducts as $row):?>
        <div class="card">
          <img src="images/<?=esc($row['image'])?>" alt="<?=esc($row['title'])?>" width="250" height="250">          
          <h1><?=e($row['title'])?></h1>
          <p class="price"><?=e($row['price'])?></p>
          <p><?=e($row['excerpt'])?></p>
          <p><a href="?p=detail&prodid=<?=e($row['id'])?>">See More</a></p>
          <form method="POST" action="?p=handleCart">
            <input type="hidden" name="id" value="<?=e($row['id'])?>">
            <input type="submit" class="add" value="Add to Cart">
        </form>
        </div>
      <?php endforeach; ?>

    </div>

    <?php 
    include __DIR__ . ('/includes/footer.php')

?>

