
<?php

function e(string $str):string
{
    return htmlentities($str, ENT_QUOTES, "UTF-8");
}
/**

* Load view with data

*

* @param string $view_name

* @param array $data

* @return void

*/

function view(string $view_name, array $data = []):void

{

try {

extract($data);

// title and content now visible in this scope

$path = __DIR__ . '/../views/' . $view_name . '.view.php';

if(!file_exists($path)) {

throw new Exception('View ' . $path . ' not found.');

}

require($path);

} catch(Exception $e) {

echo $e->getMessage();

die;

}

}

function logEvent(ILogger $logger, $event = '')
    {

    if(empty($event))
        {
            $dateFormat = (new \DateTime())->format('Y-m-d H:i:s');
            $methodRequest = $_SERVER['REQUEST_METHOD'];
            if($methodRequest === "HEAD") return;
            $URLpath = $_SERVER['REQUEST_URI'];
            $userBrowser = $_SERVER['HTTP_USER_AGENT'];
            
            $httpStatus = http_response_code();
            $event = "{$dateFormat} | {$methodRequest} | {$URLpath} | {$httpStatus} | {$userBrowser} ";
        }
    // meaningful event string:
    // data, time, page requested, users browser, or
    // if request was sucessful or not


    // invoke logger write method to save event in log
    // event will be saved in database or written to file
    // depending on type of logger int passed
    $logger->write($event);

}

function isAuth(){
    // return true if user is logged in
    // return false if not
}

function isAdmin(){
    // return true if user is logged in
    // return false if not
}

function getAllProducts($dbh){
    $query = "Select * from products";
    $stmt = $dbh->prepare($query);
    $stmt->execute();
    return $stmt->fetchAll();
}

function getProductCategories($dbh){
    $query = "Select distinct category from products";
    $stmt = $dbh->prepare($query);
    $stmt->execute();
    return $stmt->fetchAll();
}

function getProductByCategory($dbh,$cat){
    $query = "Select * from products where category=:cat";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':cat',$cat);
    $stmt->execute();
    return $stmt->fetchAll();
}

// ?? 

function getProductbyTitle($dbh, $title){
    $query = "Select * from products where title like :search or category like :search";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':search',"%$title%");
    $stmt->execute();
    return $stmt->fetchAll();

}

function getProductByID($dbh,$id){
    $query = "Select * from products where id=:id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':id',$id);
    $stmt->execute();
    return $stmt->fetchAll();
}

function getuserAddress($dbh,$id){
    $query = "Select * from users where id=:id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':id',$id);
    $stmt->execute();
    return $stmt->fetch();

}

function getCSRFToken(){
    return $_SESSION['CSRF_Token'];
}

function validateCSRFToken($CSRF):bool

        {
            if($CSRF !== getCSRFToken())
            {
                return false;
            }
            else{
                return true;
            }
        }
