<?php

$title = 'Mads Glass';
$h1 = 'log out page';

$data = compact('title', 'h1');

view('logout',$data);

// if('POST' !== $_SERVER['REQUEST_METHOD'])
// die('Unsupported request method');

if(!empty($_POST['logout'])) {
unset($_SESSION['authorized']);
$_SESSION['flash']['success'] = 'you have successfully logged out';
header('Location: login.view.php');
die;
}
  