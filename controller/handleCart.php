<?php

if(!validateCSRFToken($_POST['CSRF_Token']))

{

    die('CSRF TOKEN MISMATCH DETECTED');

}

if('POST' !== $_SERVER['REQUEST_METHOD']) die('Unsupported request method');

$query = "SELECT * FROM products WHERE id = :id";

$stmt = $dbh->prepare($query);

$stmt->bindValue(':id', $_POST['id']);

$stmt->execute();

$product = $stmt->fetch();

// if no product, return with flash message

$item = array(
    'id' => $product['id'],
    'title' => $product['title'],
    'price' => $product['price'],
    'quantity' => 1
);

$_SESSION['Cart'][$product['id']] = $item;

// flash message

$_SESSION['flash']['success'] = 'product has been added to your cart!';

header('Location: ?p=featured ');
die;