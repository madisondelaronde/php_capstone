<?php

// make sure it's a post submission

// valid all registration fields


// if errors

// redirect back to form with error
// and post in session

// end if


// insert user into database

// get last insert id

// set flash success message

// set user id in session

// header redirect to profile

// die;

if(!validateCSRFToken($_POST['CSRF_Token']))

{

    die('CSRF TOKEN MISMATCH DETECTED');

}


$first_name = (isset($_POST['first_name'])) ? $_POST['first_name'] : '';
$last_name = (isset($_POST['last_name'])) ? $_POST['last_name'] : '';
$street = (isset($_POST['street'])) ? $_POST['street'] : '';
$city = (isset($_POST['city'])) ? $_POST['city'] : '';
$postal = (isset($_POST['postal'])) ? $_POST['postal'] : '';
$province = (isset($_POST['province'])) ? $_POST['province'] : '';
$country = (isset($_POST['country'])) ? $_POST['country'] : '';
$phone = (isset($_POST['phone'])) ? $_POST['phone'] : '';
$email = (isset($_POST['email'])) ? $_POST['email'] : '';
$password = (isset($_POST['password'])) ? $_POST['password'] : '';
$password_confirm = (isset($_POST['password_confirm'])) ? $_POST['password_confirm'] : '';
$subscribe = (isset($_POST['subscribe'])?"checked='checked'":'');


// error array 

$errors = [];

// validation

if ('POST' == $_SERVER['REQUEST_METHOD']){

    if (empty($_POST['first_name'])){
        $errors['first_name'][] =  'first name is required';
    }
    if(!preg_match('/^[A-z0-9\s\-\,\']{1,32}$/', $_POST['first_name'])) {
        $errors[$first_name][] = $first_name . ' contains invalid characters';    
    }
    if (empty($_POST['last_name'])){
        $errors['last_name'][] =  'last name is required';
    }
    if(!preg_match('/^[A-z0-9\s\-\,\']{1,32}$/', $_POST['last_name'])) {
        $errors[$last_name][] = $last_name . ' contains invalid characters';   
    }
    if (empty($_POST['street'])){
        $errors['street'][] =  'street name is required';
    }
    if(!preg_match('/^[A-z0-9\s\-\,\']{1,32}$/', $_POST['street'])) {
        $errors[$street][] = $street . ' contains invalid characters';    
    }
    if (empty($_POST['city'])){
        $errors['city'][] =  'city name is required';
    }
    if(!preg_match('/^[a-zA-Z]{1,32}/', $_POST['city'])) {
        $errors[$city][] = $city . ' contains invalid characters';
    }
    if (empty($_POST['postal'])){
        $errors['postal'][] =  'postal code is required';
    }
    if(!preg_match('/^(?:[A-Z]\d[A-Z][ -]?\d[A-Z]\d)$/i', $_POST['postal'])) {
        $errors[$postal][] = $postal . ' contains invalid characters';    
    }
    if (empty($_POST['province'])){
        $errors['province'][] =  'province name is required';
    }
    if(!preg_match('/^[a-zA-Z]{1,32}/', $_POST['province'])) {
        $errors[$province][] = $province . ' contains invalid characters';   
    }
    if (empty($_POST['country'])){
        $errors['country'][] =  'country name is required';
    }
    if(!preg_match('/^[a-zA-Z]{1,32}/', $_POST['country'])) {
        $errors[$country][] = $country . ' contains invalid characters';    
    }
    if (empty($_POST['phone'])){
        $errors['phone'][] =  'phone number is required';
    }
    if(!preg_match('/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/', $_POST['phone'])) {
        $errors[$phone][] = $phone . ' contains invalid characters';    
    }
    // email sanitation and validation
    if (empty($_POST['email'])){
        $errors['email'][] =  'email is required';
    }
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[$email][] = $email . ' contains invalid characters';    
    }
    if (empty($_POST['password'])){
        $errors['password'][] =  'password is required';
    }
    if ($_POST['password']< 6){
        $errors[$password][] = $password . ' must be over 6 characters';    
    }
    if (empty($_POST['password_confirm'])){
        $errors['password_confirm'][] =  'password confirmation is required';
    }
    if ($_POST['password_confirm'] !== $_POST['password']){
        $errors[$password_confirm][] = 'passwords must match';    
    }
    if (!isset($_POST['subscribe'])){
    $errors['subscribe'] = "subscription required";
    }

    // if zero errors insert data and redirect

    if(count($errors) == 0) {

//check if email exists
$qq = "select * from users where email=:email";
$stmt = $dbh->prepare($qq);
$stmt->bindValue(':email', $_POST['email']);
$stmt->execute();
if(!empty($stmt->fetch())){
    $_SESSION['post'] = json_encode($_POST);
    $_SESSION['flash']['error'] = "Sorry this email exists already.";
    header("Location: /?=register");
    die;
}

        $query = "INSERT INTO users
                  (
                      first_name, 
                      last_name,
                      street,
                      city,
                      postal,
                      province,
                      country,
                      phone,
                      email, 
                      subscribe,
                      password
                   )
                   VALUES
                   (
                      :first_name,
                      :last_name,
                      :street,
                      :city,
                      :postal,
                      :province,
                      :country,
                      :phone,
                      :email, 
                      :subscribe,
                      :password
                   )";
    
      $stmt = $dbh->prepare($query);
    
      $stmt->bindValue(':first_name', $_POST['first_name']);
      $stmt->bindValue(':last_name', $_POST['last_name']);
      $stmt->bindValue(':street', $_POST['street']);
      $stmt->bindValue(':city', $_POST['city']);
      $stmt->bindValue(':postal', $_POST['postal']);
      $stmt->bindValue(':province', $_POST['province']);
      $stmt->bindValue(':country', $_POST['country']);
      $stmt->bindValue(':phone', $_POST['phone']);
      $stmt->bindValue(':email', $_POST['email']);
      $stmt->bindValue(':subscribe', $_POST['subscribe']);

      $password_hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
      $stmt->bindValue(':password', $password_hash);
      
        $stmt->execute();
    
        $id = $dbh->lastInsertId();
    
        if($id) {
            $_SESSION['user_id'] = $id;
            $_SESSION['flash']['success'] = "registration successful";
            header("Location: /?p=submitted&id=" . e($id));
            die;
        } else {
            header("Location: /?=register");
            $_SESSION['flash']['error'] = "there was a problem inserting your information";
            die;
        }   
    }
    else {
        $_SESSION['post'] = json_encode($_POST);
        $_SESSION['errors'] = json_encode($errors);
        header('Location: ?p=register');
        die;
    }
    
    }

    header('Location: /?p=submitted');