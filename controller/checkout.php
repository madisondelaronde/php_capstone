<?php

$title = 'Checkout';

if(empty($_SESSION['Cart'])){
    $_SESSION['flash']['error'] = 'sorry, your cart is empty';
    header('Location: ?p=featured');
    die;
}

if(empty($_SESSION['user_id'])){
    $_SESSION['flash']['error'] = 'you must be logged in to checkout';
    $_SESSION['target'] = '?p=checkout';
    header('Location: ?p=login');
    die;
}

$pst = 0;
$gst = 0;
$subtotal = 0;
$total = 0;


$flash = !empty($_SESSION['flash']) ? $_SESSION['flash'] : [];

unset($_SESSION['flash']);

if(!empty($_SESSION['Cart'])){
    foreach($_SESSION['Cart'] as $row){
        $subtotal += $row['price'];
    }
    $pst = 0.07 * $subtotal;
    $gst = 0.05 * $subtotal;
    $total = $gst + $pst + $subtotal;
}

$errors = !empty($_SESSION['errors']) ? json_decode($_SESSION['errors'],true) : [];
unset($_SESSION['errors']);

$post = !empty($_SESSION['post']) ? json_decode($_SESSION['post'],true) : [];
unset($_SESSION['post']);

$data = compact('title', 'flash', 'gst', 'pst', 'subtotal', 'total', 'errors','post');


view('checkout',$data);