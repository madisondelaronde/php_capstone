<?php 
    
$title = 'Details';

if(str_contains($_SERVER['QUERY_STRING'] , 'prodid')){

    parse_str($_SERVER['QUERY_STRING'],$urlquery);
    $productID = $urlquery['prodid'];
    $detailData = getProductByID($dbh,$productID);

}else{
    header('Location: ?p=featured');
    die();
}

$data = compact('title','detailData');


view('detail',$data);