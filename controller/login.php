<?php

$title = 'Mads Glass';
$h1 = 'log in page';

$flash = !empty($_SESSION['flash']) ? $_SESSION['flash'] : [];

unset($_SESSION['flash']);

$data = compact('title', 'h1', 'flash');

view('login',$data);