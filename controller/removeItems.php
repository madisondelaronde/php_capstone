<?php

if(!empty($_GET['clear']))
{
    unset($_SESSION['Cart']);

    $_SESSION['flash']['success'] = 'all products have been removed from cart';
    header('Location: ?p=viewCart');
    die;
}

if('POST' !== $_SERVER['REQUEST_METHOD']) die('Unsupported request method');

if(!empty($_POST['id']))
{
    unset($_SESSION['Cart'][$_POST['id']]);

    $_SESSION['flash']['success'] = 'product has been removed from cart';
    header('Location: ?p=viewCart');
    die;
}
