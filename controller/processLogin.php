<?php


if('POST' !== $_SERVER['REQUEST_METHOD']) {
    http_status_code(405);
    die('405 - Unsupported Request Method');
}

if(!validateCSRFToken($_POST['CSRF_Token']))

{

    die('CSRF TOKEN MISMATCH DETECTED');

}


// This should be in your user model
global $dbh;

$query = "SELECT * FROM users WHERE email = :email ";

$stmt = $dbh->prepare($query);

$stmt->bindValue(':email', $_POST['email']);

$stmt->execute();

$user = $stmt->fetch(PDO::FETCH_ASSOC);


if(!$user || !password_verify($_POST['password'], $user['password'])) {

    $_SESSION['flash']['error'] = 'sorry, those credentials do not match our records.';
    header('Location: /?p=login');
    die;
}

if($user['is_admin'] == 1){

    $_SESSION['flash']['success'] = 'congrats on logging in, admin!!!!!!!';
    $_SESSION['user_id'] = $user['id']??0;
    $_SESSION['admin'] = 1;
    if(!empty($_SESSION['target'])){
        header('Location: /' . $_SESSION['target']);
        die;
    }
    
    header('Location: /admin');
    die;
     
}

$_SESSION['flash']['success'] = 'congratulations, you are logged in!';
$_SESSION['user_id'] = $user['id']??0;

if(!empty($_SESSION['target'])){
    header('Location: /' . $_SESSION['target']);
    die;
}

header('Location: /?p=profile');
die;
