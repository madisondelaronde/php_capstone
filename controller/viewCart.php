<?php 

$pst = 0;
$gst = 0;
$subtotal = 0;
$total = 0;


    
$title = 'Cart Details';

$flash = !empty($_SESSION['flash']) ? $_SESSION['flash'] : [];

unset($_SESSION['flash']);

if(!empty($_SESSION['Cart'])){
    foreach($_SESSION['Cart'] as $row){
        $subtotal += $row['price'];
    }
    $pst = 0.07 * $subtotal;
    $gst = 0.05 * $subtotal;
    $total = $gst + $pst + $subtotal;
}

$data = compact('title', 'flash', 'gst', 'pst', 'subtotal', 'total');


view('viewCart',$data);