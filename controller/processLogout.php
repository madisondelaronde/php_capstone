<?php


// if('POST' !== $_SERVER['REQUEST_METHOD']) {
//     http_status_code(405);
//     die('405 - Unsupported Request Method');
// }


// if(!empty($_POST['logout'])) {
if(!empty($_SESSION['user_id'])){
    unset($_SESSION['user_id']);
    unset($_SESSION['Cart']);
    unset($_SESSION);
    session_regenerate_id();
    $_SESSION['flash']['success'] = 'You have successfully logged out';
    header('Location: /?p=logout');
    die;   
}

// $_SESSION['flash']['error'] = 'Are you sure you are logged in?';
//     header('Location: login.php');
//     die;   
