<?php
 $title = 'Featured';

//loop through to show list of products using static view as template
 $allProducts =getAllProducts($dbh);

if(!empty($_GET['search'])){

    $allProducts = getProductByTitle($dbh, $_GET['search']);
 
}

if(!empty($_GET['category'])){

    $allProducts = getProductByCategory($dbh, $_GET['category']);
 
}

$allcategories = getProductCategories($dbh);

$flash = !empty($_SESSION['flash']) ? $_SESSION['flash'] : [];

unset($_SESSION['flash']);

$data = compact('title', 'allProducts', 'allcategories', 'flash');

 
 view('featured',$data);

