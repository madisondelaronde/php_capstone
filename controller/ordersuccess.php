<?php

if(empty($_SESSION['checkoutDone'])){
    header('Location: /?p=featured');
}
else {
    unset($_SESSION['checkoutDone']);
    $invoiceId = $_SESSION['checkout_invoiceid'];
    unset($_SESSION['checkout_invoiceid']);
}
$title = 'Order Successful';

$finalMsg ="Order placed successfully.";

$flash = !empty($_SESSION['flash']) ? $_SESSION['flash'] : [];

unset($_SESSION['flash']);

$query = "SELECT invoice.*,
    users.first_name,
    users.last_name,
    users.email,
    users.postal,
    users.street,
    users.city,
    line_items.price as line_price,
    line_items.quantity,
    line_items.title as line_title
    
    from invoice
    join users on invoice.user_id = users.id
    join line_items on invoice.id = line_items.invoice_id
    
    where
    invoice.id = :id
    ";
$stmt =$dbh->prepare($query);
$stmt->bindValue(':id',$invoiceId);
$stmt->execute();

$invoice_items = $stmt->fetchAll();


$pst = 0;
$gst = 0;
$subtotal = 0;
$total = 0;
foreach($invoice_items as $row){
    $subtotal += $row['price'];
}
$pst = 0.07 * $subtotal;
$gst = 0.05 * $subtotal;
$total = $gst + $pst + $subtotal;

$data = compact('title','finalMsg','flash','invoice_items','pst','gst','total',
'subtotal');

view('ordersuccess',$data);