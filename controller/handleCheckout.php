<?php

if(!validateCSRFToken($_POST['CSRF_Token']))

{

    die('CSRF TOKEN MISMATCH DETECTED');

}

unset($_SESSION['target']);

$errors = [];

if('POST' !== $_SERVER['REQUEST_METHOD']) {
    http_status_code(405);
    die('405 - Unsupported Request Method');
}

if (empty($_POST['cardName'])){
    $errors['cardName'][] =  'card name is required';
}
if(!preg_match('/^[A-z0-9\s\-\,\']{1,32}$/', $_POST['cardName'])) {
    $errors[$cardName][] = $cardName . ' contains invalid characters';    
}

if (empty($_POST['cardNumber'])){
    $errors['cardNumber'][] =  'card number is required';
}
if(!preg_match('/^[1-9]\d*$/', $_POST['cardNumber'])) {
    $errors[$cardNumber][] = $cardNumber . ' contains invalid characters';    
}
if(strlen($_POST['cardNumber'])!= 16){
    $errors[$cardNumber][] = $cardNumber . ' must be 16 characters';    
}

if (empty($_POST['expiry'])){
    $errors['expiry'][] =  'expiry date is required';
}
if(!preg_match('/^[1-9]\d*$/', $_POST['expiry'])) {
    $errors[$expiry][] = $expiry . ' contains invalid characters';    
}
if(strlen($_POST['expiry'])!= 4){
    $errors[$expiry][] = $expiry . ' must be 4 characters';    
}

if (empty($_POST['cvv'])){
    $errors['cvv'][] =  'CVV is required';
}
if(!preg_match('/^[1-9]\d*$/', $_POST['cvv'])) {
    $errors[$cvv][] = $cvv . ' contains invalid characters';    
}
if(strlen($_POST['cvv'])!= 3){
    $errors[$cvv][] = $cvv . ' must be 3 characters';    
}

if(count($errors) != 0) {
    $_SESSION['errors'] = json_encode($errors);
    $_SESSION['post'] = json_encode($_POST);
    header('Location: ?p=checkout');
    die;
}
else {
    // insert cart items into invoice

    if(!empty($_SESSION['Cart'])){

        $subtotal = 0;

        foreach($_SESSION['Cart'] as $row){
            $subtotal += $row['price'];
        }
        $pst = 0.07 * $subtotal;
        $gst = 0.05 * $subtotal;
        $total = $gst + $pst + $subtotal;
    }

    try{
        $dbh->beginTransaction();
        $query = "INSERT into invoice
                (
                    user_id,
                price,
                address,
                pst,
                gst,
                subtotal 
                ) values
                (:user,
                :price,
                :address,
                :pst,
                :gst,
                :subtotal
                )";
        
        $address = getuserAddress($dbh,$_SESSION['user_id']);

        $stmt = $dbh->prepare($query);
        $stmt->bindValue(':user',$_SESSION['user_id']);
        $stmt->bindValue(':price',$total);
        $stmt->bindValue(':address',json_encode($address));
        $stmt->bindValue(':pst',$pst);
        $stmt->bindValue(':gst',$gst);
        $stmt->bindValue(':subtotal',$subtotal);

        $stmt->execute();
        $invoiceId= $dbh->lastInsertId();

        
        $query2 = "INSERT INTO line_items
                (
                    invoice_id,
                    product_id,
                    title,
                    price,
                    quantity
                    )
                     values
                    (
                        :invoiceid,
                    :prodid,
                    :title,
                    :price,
                    :quantity
                    )";
            
            $stmt2 = $dbh->prepare($query2);

            foreach($_SESSION['Cart'] as $row){
                $stmt2->bindValue(':invoiceid',$invoiceId);
                $stmt2->bindValue(':price',$row['price']);
                $stmt2->bindValue(':prodid',$row['id']);
                $stmt2->bindValue(':title',$row['title']);
                $stmt2->bindValue(':quantity',$row['quantity']);
                $stmt2->execute();
            }

            $dbh->commit();

            unset($_SESSION['Cart'] );
            $_SESSION['checkoutDone'] = 1;
            $_SESSION['checkout_invoiceid'] = $invoiceId;

        
            //insertion complete. show success message
            $_SESSION['flash']['success'] = 'Your order has been placed';

    header('Location: ?p=ordersuccess ');
die;


    }catch(\PDOException $e){

        $dbh->rollBack();
        
        echo $e->getMessage();
        
        die;
        
        }
}